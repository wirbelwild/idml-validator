<?php

/**
 * IDML-Validator
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <tobias.koengeter@bitandblack.com>
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace IDML\Validator;

use BitAndBlack\Composer\VendorPath;
use BitAndBlack\PathInfo\PathInfo;
use Symfony\Component\String\Inflector\EnglishInflector;

/**
 * Class Schema
 *
 * @package IDML\Validator
 * @see \IDML\Validator\Tests\SchemaTest
 */
class Schema
{
    /**
     * @var array<string, string>
     */
    private array $files = [];

    /**
     * @var string
     */
    private string $suffix = 'xsd';

    /**
     * Schema constructor.
     */
    public function __construct()
    {
        $schemaFolder = dirname(new VendorPath()) . DIRECTORY_SEPARATOR . 'schema';
        $xsdFiles = glob($schemaFolder . DIRECTORY_SEPARATOR . '*.' . $this->suffix);

        if (!is_array($xsdFiles)) {
            return;
        }

        /**
         * @var string $xsdFile
         */
        foreach ($xsdFiles as $xsdFile) {
            $fileName = basename($xsdFile, '.' . $this->suffix);
            $fileName = mb_strtolower($fileName);
            $this->files[$fileName] = $xsdFile;
        }
        
        ksort($this->files);
    }

    /**
     * Looks for the folder name and the file name and tries to find the key word for the xsd file.
     *
     * @param string $entryName
     * @return string|null
     */
    public function getSchemaFromEntryName(string $entryName): ?string
    {
        $entryName = mb_strtolower($entryName);
        
        $names = explode('/', $entryName);
        $folderName = $names[0];
        $fileName = $names[1] ?? null;

        $key = $fileName ?? $folderName;
        $key = (string) PathInfo::createFromFile($key)->getFileName();

        $inflector = new EnglishInflector();
        $folderNamesSingular = $inflector->singularize($folderName);
        
        $folderNameSingular = $folderNamesSingular[count($folderNamesSingular) - 1];

        if (0 === strpos($key, (string) $folderNameSingular)) {
            $key = $folderNameSingular;
        }
        
        if (!array_key_exists($key, $this->files)) {
            return null;
        }
        
        return $this->files[$key];
    }
}
