<?php

/**
 * IDML-Validator
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <tobias.koengeter@bitandblack.com>
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace IDML\Validator;

use BitAndBlack\Unzip\Type\FileType;

/**
 * Class File
 *
 * @package IDML\Validator
 */
class File extends FileType
{
}
