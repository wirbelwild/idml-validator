<?php

/**
 * IDML-Validator
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <tobias.koengeter@bitandblack.com>
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace IDML\Validator\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class WelcomeCommand
 *
 * @package IDML\Validator\Commands
 */
class WelcomeCommand extends Command
{
    /**
     * Configuration
     */
    protected function configure(): void
    {
        $this
            ->setName('welcome')
            ->setDescription('Displays the welcome message.')
            ->setHelp('This command displays the welcome message.')
        ;
    }

    /**
     * Displays a welcome message
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Welcome to the IDML Validator.');
        return 0;
    }
}
