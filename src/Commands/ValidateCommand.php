<?php

/**
 * IDML-Validator
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <tobias.koengeter@bitandblack.com>
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace IDML\Validator\Commands;

use BitAndBlack\Unzip\Exception\CouldNotReadFileException;
use IDML\Validator\Error;
use IDML\Validator\File;
use IDML\Validator\Schema;
use IDML\Validator\Validation;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Terminal;

/**
 * Class ValidateCommand
 *
 * @package IDML\Validator\Commands
 */
class ValidateCommand extends Command
{
    /**
     * Configuration
     */
    protected function configure(): void
    {
        $this
            ->setName('validate')
            ->setDescription('Validates an IDML file.')
            ->setHelp('This command validates an IDML file by a given schema.')
            ->addArgument(
                'file',
                InputArgument::REQUIRED,
                'Path to the IDML file you want to validate.'
            )
            ->addOption(
                'memory-limit-disabled',
                null,
                InputOption::VALUE_NONE,
                'Disable the memory limit to parse huge files'
            )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws CouldNotReadFileException
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $memoryLimitDisabled = $input->getOption('memory-limit-disabled');
        
        if ($memoryLimitDisabled && !ini_set('memory_limit', '-1')) {
            $output->writeln('<error>Can\'t set memory limit.</error>');
        }
        
        $memoryLimit = ini_get('memory_limit');
        
        $output->writeln('Running. Memory limit: ' . ((float) $memoryLimit === -1.0 ? 'disabled' : $memoryLimit) . '.');

        $filePath = $input->getArgument('file');

        if (!is_string($filePath)) {
            $output->writeln('<error>Input "file" is malformed.</error>');
            return Command::FAILURE;
        }

        $idmlFile = new File($filePath);
        
        $validation = new Validation(
            $idmlFile,
            new Schema(),
            $output
        );
        
        $validation->validate();
        $errorsAll = $validation->getErrors();
        
        $terminal = new Terminal();
        $widthTerminal = $terminal->getWidth();
        
        if ($widthTerminal > 120) {
            $widthTerminal = 120;
        }
        
        $maxWidthFirstColumn = 5;
        
        $output->writeln('Done parsing. Found ' . count($errorsAll) . ' errors.');
        
        foreach ($errorsAll as $fileName => $errors) {
            $output->writeln(PHP_EOL . '<info>' . $fileName . '</info>');
            $output->writeln('Found ' . count($errors) . ' errors:');
            
            $table = new Table($output);
            $table
                ->setHeaderTitle((string) $fileName)
                ->setHeaders([
                    'Line',
                    'Message',
                ])
                ->setColumnMaxWidth(0, $maxWidthFirstColumn)
                ->setColumnMaxWidth(1, $widthTerminal - $maxWidthFirstColumn - 3 - 3)
                ->setStyle('box')
            ;

            $lastElement = end($errors);
            
            /**
             * @var Error $error
             */
            foreach ($errors as $error) {
                $table->addRow([
                    $error->getLine(),
                    $error->getMessage(),
                ]);
                
                if ($error !== $lastElement) {
                    $table->addRow(new TableSeparator());
                }
            }
            
            $table->render();
        }
        
        return Command::SUCCESS;
    }
}
