<?php

/**
 * IDML-Validator
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <tobias.koengeter@bitandblack.com>
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace IDML\Validator;

use LibXMLError;

/**
 * Class Error
 *
 * @package IDML\Validator
 */
class Error
{
    /**
     * @var int
     */
    private int $level;
    
    /**
     * @var string
     */
    private string $message;
    
    /**
     * @var int
     */
    private int $line;
    
    /**
     * @var string
     */
    private string $file;
    
    /**
     * @var int
     */
    private int $code;

    /**
     * Error constructor.
     *
     * @param LibXMLError $error
     * @param string $fileName
     */
    public function __construct(LibXMLError $error, string $fileName)
    {
        $this->level = $error->level;
        $this->code = $error->code;
        $this->message = trim($error->message);
        $this->line = $error->line;
        $this->file = $fileName;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return int
     */
    public function getLine(): int
    {
        return $this->line;
    }

    /**
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }
}
