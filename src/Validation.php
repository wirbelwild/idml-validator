<?php

/**
 * IDML-Validator
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <tobias.koengeter@bitandblack.com>
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace IDML\Validator;

use BitAndBlack\Unzip\Type\FileType;
use DOMDocument;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Validation
 *
 * @package IDML\Validator
 */
class Validation implements LoggerAwareInterface
{
    private FileType $idmlFile;
    
    /**
     * @var Schema
     */
    private Schema $schema;

    /**
     * @var array<int|string, array<int, Error>>
     */
    private array $errors = [];
    
    /**
     * @var OutputInterface
     */
    private OutputInterface $output;

    private LoggerInterface $logger;

    /**
     * Validation constructor.
     *
     * @param FileType $idmlFile
     * @param Schema $schema
     * @param OutputInterface|null $output
     * @param LoggerInterface|null $logger
     */
    public function __construct(
        FileType $idmlFile,
        Schema $schema,
        OutputInterface $output = null,
        LoggerInterface $logger = null
    ) {
        $this->idmlFile = $idmlFile;
        $this->schema = $schema;
        $this->output = $output ?? new NullOutput();
        $this->logger = $logger ?? new NullLogger();
    }

    /**
     * @param LoggerInterface $logger
     * @return void
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    /**
     * @return bool
     */
    public function validate(): bool
    {
        $handledElements = 0;
        $contents = $this->idmlFile->getContents();
        $contentsCount = count($contents);
        
        $this->output->writeln('Found ' . $contentsCount . ' files in ' . $this->idmlFile->getFile() . '.');
        
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] %message%');
        $progressBar = new ProgressBar($this->output, $contentsCount);
        $progressBar->setFormat('custom');
        $progressBar->setMessage('Loading now...');
        $progressBar->start();
        
        foreach ($contents as $entryName => $entryContent) {
            $progressBar->setMessage('Validating ' . $entryName);
            $progressBar->advance();
            $this->logger->debug('Handling file "' . $entryName . '".');
            
            if (0 !== strpos($entryContent, '<?xml')) {
                $this->logger->debug('Not containing XML content. Ignoring.');
                continue;
            }
            
            $domDocument = new DOMDocument();
            $domDocument->loadXML($entryContent, LIBXML_PARSEHUGE);

            $schema = $this->schema->getSchemaFromEntryName($entryName);
            
            if (null === $schema) {
                $this->logger->debug('No schema found, aborting.');
                continue;
            }

            $this->logger->debug('Validating against schema "' . $schema . '".');

            libxml_use_internal_errors(true);

            if (!$domDocument->schemaValidate($schema)) {
                $errors = $this->getValidationErrors($entryName);
                $this->errors[$entryName] = $errors;
                $this->logger->debug('Found ' . count($errors) . ' errors.');
            }
            
            libxml_use_internal_errors(false);
            ++$handledElements;
        }
        
        $progressBar->finish();
        $this->output->writeln('');
        
        return 0 !== $handledElements;
    }

    /**
     * @param string $fileName
     * @return array<int, Error>
     */
    private function getValidationErrors(string $fileName): array
    {
        $errors = [];

        foreach (libxml_get_errors() as $error) {
            $errors[] = new Error($error, $fileName);
        }

        libxml_clear_errors();
        return $errors;
    }

    /**
     * @return array<int|string, array<int, Error>>
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
