<?php

/**
 * IDML-Validator
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <tobias.koengeter@bitandblack.com>
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace IDML\Validator\Tests;

use Generator;
use IDML\Validator\Schema;
use PHPUnit\Framework\TestCase;

/**
 * Class SchemaTest
 *
 * @package IDML\Validator\Tests
 */
class SchemaTest extends TestCase
{
    /**
     * @param string $file
     * @dataProvider getXMLFiles
     */
    public function testCanLoadXSD(string $file): void
    {
        $schema = new Schema();
        
        self::assertFileExists(
            (string) $schema->getSchemaFromEntryName($file),
            $file
        );
    }

    /**
     * @return Generator<array<int, string>>
     */
    public static function getXMLFiles(): Generator
    {
        yield ['designmap.xml'];
        yield ['Masterspreads/Masterspread_5000.xml'];
        yield ['Resources/Fonts.xml'];
        yield ['Resources/Graphic.xml'];
        yield ['Resources/Preferences.xml'];
        yield ['Resources/Styles.xml'];
        yield ['Spreads/Spread_5000.xml'];
        yield ['XML/Backingstory.xml'];
        yield ['XML/Tags.xml'];
    }
}
