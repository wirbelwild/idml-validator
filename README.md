[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/idml-validator)](http://www.php.net)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/d18c9047422341e78e77c32503118035)](https://www.codacy.com/bb/wirbelwild/idml-validator/dashboard) 
[![Total Downloads](https://poser.pugx.org/bitandblack/idml-validator/downloads)](https://packagist.org/packages/bitandblack/idml-validator)
[![License](https://poser.pugx.org/bitandblack/idml-validator/license)](https://packagist.org/packages/bitandblack/idml-validator)

# IDML Validator

Validates IDML files by the official schema from Adobe.

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/bitandblack/idml-validator). Add it to your project by running `$ composer require bitandblack/idml-validator`.

## Usage 

The IDML Validator is mainly meant to be used by CLI. Nevertheless it can also be integrated in a PHP project.

### Execute a validation via CLI

Run `$ php vendor/bin/idml-validator validate /path/to/file.idml` to validate your IDML file. 

This will print something like:

````console
Found 3 errors:
┌──────┬───────────────────────── designmap.xml ────────────────────────────────┐
│ Line │ Message                                                                │
├──────┼────────────────────────────────────────────────────────────────────────┤
│ 3    │ Element 'Document', attribute 'DOMVersion': [facet 'enumeration'] The  │
│      │ value '14.0' is not an element of the set {'8.0'}.                     │
├──────┼────────────────────────────────────────────────────────────────────────┤
│ 3    │ Element 'Document', attribute 'DOMVersion': '14.0' is not a valid valu │
│      │ e of the local atomic type.                                            │
├──────┼────────────────────────────────────────────────────────────────────────┤
│ 23   │ Element 'EndnoteOption': This element is not expected. Expected is one │
│      │ of ( TextVariable, {http://ns.adobe.com/AdobeInDesign/idml/1.0/packagi │
│      │ ng}Tags, Layer, {http://ns.adobe.com/AdobeInDesign/idml/1.0/packaging} │
│      │ MasterSpread, {http://ns.adobe.com/AdobeInDesign/idml/1.0/packaging}Sp │
│      │ read, Section, DocumentUser, CrossReferenceFormat, Index, {http://ns.a │
│      │ dobe.com/AdobeInDesign/idml/1.0/packaging}BackingStory ).              │
└──────┴────────────────────────────────────────────────────────────────────────┘
````

### Custom 

For an integrated use of the validator, take the `Validation` class, give it a file and a schema: 

````php
<?php

use IDML\Validator\File;
use IDML\Validator\Schema;
use IDML\Validator\Validation;

$validation = new Validation(
    new File('/path/to/file.idml'),
    new Schema()
);

$validation->validate();
$errors = $validation->getErrors();
````

## One problem

A huge problem is that Adobe hasn't updated the schema since InDesign CS6. So every newer function will end in an error, even when it's official possible. We're talking to Adobe and waiting for an answer at the moment. So be careful about the errors and don't mind when you believe a function is possible but throws an error. 

## Help 

If you have any questions feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).